# Firebase Web Codelab - Start code

This folder contains the starting code for the [Firebase: Build a Real Time Web Chat App Codelab](https://codelabs.developers.google.com/codelabs/firebase-web/).

If you'd like to jump directly to the end and see the finished code head to the [web](../web) directory.


# Emotiquiz

A simple WebUI frontend for an app called Emotiquiz. It's a quiz app that uses Firebase as its data storage. The frontend then uses the data and displays it in a user-friendly way.

What it shows:

    - Previous users(the users that have been active recently)
    - Highest scoring users(users with the highest total score)
    - Graphs for random things to please the eyes

## The database structure

The database has a complicated structure, because it's firebase. In json form, it looks a bit like this:

```json
{
  "88GxO7qtZaVdTwvt4zvGtbEvaf33" : {
    "RANKED" : {
      "042eafa9-2b47-4691-a23d-df598f95a43a" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 27,
          "month" : 11,
          "nanos" : 550000000,
          "seconds" : 44,
          "time" : 1544603264550,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -6755998290231843782,
        "mostSignificantBits" : 301371366190827153,
        "score" : 0
      },
      "53bb309e-9758-4893-b807-b1131ca9efda" : {
        "Level 1 - Question 1" : {
          "answer" : "DISGUST",
          "correct" : "DISGUST",
          "result" : "correct"
        },
        "Level 1 - Question 2" : {
          "answer" : "NEUTRAL_SLOW",
          "correct" : "NEUTRAL_SLOW",
          "result" : "correct"
        },
        "Level 1 - Question 3" : {
          "answer" : "NEUTRAL_FAST",
          "correct" : "JOY",
          "result" : "wrong"
        },
        "date" : {
          "date" : 11,
          "day" : 2,
          "hours" : 22,
          "minutes" : 9,
          "month" : 11,
          "nanos" : 131000000,
          "seconds" : 15,
          "time" : 1544562555131,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5185981750250442790,
        "mostSignificantBits" : 6033469583495022739,
        "score" : 40
      },
      "6046c80a-5460-46ab-9341-b0b1da9a9e16" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 32,
          "month" : 11,
          "nanos" : 364000000,
          "seconds" : 13,
          "time" : 1544603533364,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -7835787598724686314,
        "mostSignificantBits" : 6937452222701651627,
        "score" : 0
      },
      "7344c8f6-b1f7-4a25-9a9c-db67afd16fba" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 25,
          "month" : 11,
          "nanos" : 771000000,
          "seconds" : 19,
          "time" : 1544603119771,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -7305723257123934278,
        "mostSignificantBits" : 8305984574651320869,
        "score" : 0
      },
      "a06e76eb-bae8-4db8-addd-ce1b5a820699" : {
        "Level 1 - Question 1" : {
          "answer" : "DISGUST",
          "correct" : "DISGUST",
          "result" : "correct"
        },
        "Level 1 - Question 2" : {
          "answer" : "FEAR",
          "correct" : "DISGUST",
          "result" : "wrong"
        },
        "Level 1 - Question 3" : {
          "answer" : "JOY",
          "correct" : "SURPRISE",
          "result" : "wrong"
        },
        "Level 1 - Question 4" : {
          "answer" : "ANGER",
          "correct" : "ANGER",
          "result" : "correct"
        },
        "date" : {
          "date" : 11,
          "day" : 2,
          "hours" : 22,
          "minutes" : 10,
          "month" : 11,
          "nanos" : 895000000,
          "seconds" : 30,
          "time" : 1544562630895,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5918347718417054055,
        "mostSignificantBits" : -6886436025377731144,
        "score" : 40
      },
      "e401592c-0318-4b08-b4cc-e4da1de57fea" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 26,
          "month" : 11,
          "nanos" : 445000000,
          "seconds" : 45,
          "time" : 1544603205445,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5418704626177966102,
        "mostSignificantBits" : -2017233112519914744,
        "score" : 0
      },
      "totalScore" : 80
    },
    "country" : "United Kingdom",
    "email" : "zosiarytlewska@gmail.com",
    "metadata" : {
      "creationTimestamp" : 1540057795000,
      "lastSignInTimestamp" : 1544603187000
    },
    "name" : "Zofia"
  },
  "TVDXUeO8AGOY0ZXyRdtYjN6wK8V2" : {
    "PRACTICE" : {
      "b2d91ad4-ee3b-41d1-b921-765097233759" : {
        "Level 1 - Question 1" : {
          "answer" : "FEAR",
          "correct" : "SURPRISE",
          "result" : "wrong"
        },
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 40,
          "month" : 11,
          "nanos" : 884000000,
          "seconds" : 11,
          "time" : 1544604011884,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5106670413956303015,
        "mostSignificantBits" : -5559382763179916847
      },
      "b5adc29b-e556-429a-8ab1-3b57f1a0f1f6" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 40,
          "month" : 11,
          "nanos" : 528000000,
          "seconds" : 1,
          "time" : 1544604001528,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -8452909776695660042,
        "mostSignificantBits" : -5355410407050296678
      }
    },
    "RANKED" : {
      "07a1c168-1ea9-4e7b-8225-b0e845fc4ddf" : {
        "Level 1 - Question 1" : {
          "answer" : "DISGUST",
          "correct" : "DISGUST",
          "result" : "correct"
        },
        "date" : {
          "date" : 11,
          "day" : 2,
          "hours" : 21,
          "minutes" : 59,
          "month" : 11,
          "nanos" : 290000000,
          "seconds" : 45,
          "time" : 1544561985290,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -9068647762987561505,
        "mostSignificantBits" : 549933282451082875,
        "score" : 0
      },
      "1a1246bf-40ef-4141-b403-24907a283631" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 35,
          "month" : 11,
          "nanos" : 62000000,
          "seconds" : 34,
          "time" : 1544603734062,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5475492519009044943,
        "mostSignificantBits" : 1878641781809037633,
        "score" : 0
      },
      "317a7fd2-beb4-4764-982a-b62f397e6b8f" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 41,
          "month" : 11,
          "nanos" : 789000000,
          "seconds" : 3,
          "time" : 1544604063789,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -7481967516978353265,
        "mostSignificantBits" : 3565302598136514404,
        "score" : 0
      },
      "34e48f8e-163d-405d-88e3-7554bc366f3b" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 39,
          "month" : 11,
          "nanos" : 604000000,
          "seconds" : 38,
          "time" : 1544603978604,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -8582887458042646725,
        "mostSignificantBits" : 3811329025083523165,
        "score" : 0
      },
      "3b0f0962-d618-4c86-8c06-39cdcda7e82c" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 44,
          "month" : 11,
          "nanos" : 391000000,
          "seconds" : 15,
          "time" : 1544604255391,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -8356928502457964500,
        "mostSignificantBits" : 4255630492991769734,
        "score" : 0
      },
      "40067f98-e6ae-4a21-90da-9dc537c01ca3" : {
        "Level 1 - Question 1" : {
          "answer" : "FEAR",
          "correct" : "FEAR",
          "result" : "correct"
        },
        "Level 1 - Question 2" : {
          "answer" : "DISGUST",
          "correct" : "FEAR",
          "result" : "wrong"
        },
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 10,
          "minutes" : 13,
          "month" : 11,
          "nanos" : 487000000,
          "seconds" : 55,
          "time" : 1544606035487,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -8008915516955550557,
        "mostSignificantBits" : 4613515162969590305,
        "score" : 20
      },
      "61e7d730-0c0e-42ca-863a-be3e42d7a56b" : {
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 36,
          "month" : 11,
          "nanos" : 839000000,
          "seconds" : 34,
          "time" : 1544603794839,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -8774491749359311509,
        "mostSignificantBits" : 7054843942659834570,
        "score" : 0
      },
      "8d5c341e-d5d0-4830-913e-5618052d8b2a" : {
        "Level 1 - Question 1" : {
          "answer" : "DISGUST",
          "correct" : "NEUTRAL_FAST",
          "result" : "wrong"
        },
        "Level 1 - Question 2" : {
          "answer" : "SADNESS",
          "correct" : "NEUTRAL_SLOW",
          "result" : "wrong"
        },
        "Level 1 - Question 3" : {
          "answer" : "SADNESS",
          "correct" : "SURPRISE",
          "result" : "wrong"
        },
        "Level 1 - Question 4" : {
          "answer" : "NEUTRAL_FAST",
          "correct" : "NEUTRAL_SLOW",
          "result" : "wrong"
        },
        "Level 1 - Question 5" : {
          "answer" : "SURPRISE",
          "correct" : "NEUTRAL_SLOW",
          "result" : "wrong"
        },
        "Level 1 - Question 6" : {
          "answer" : "NEUTRAL_SLOW",
          "correct" : "NEUTRAL_FAST",
          "result" : "wrong"
        },
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 9,
          "minutes" : 41,
          "month" : 11,
          "nanos" : 163000000,
          "seconds" : 18,
          "time" : 1544604078163,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -7980846828487865558,
        "mostSignificantBits" : -8260670309463472080,
        "score" : 0
      },
      "8fec2e0f-fb73-40fe-b5db-7514b9817d1a" : {
        "Level 1 - Question 1" : {
          "answer" : "NEUTRAL_FAST",
          "correct" : "NEUTRAL_FAST",
          "result" : "correct"
        },
        "Level 1 - Question 2" : {
          "answer" : "NEUTRAL_FAST",
          "correct" : "JOY",
          "result" : "wrong"
        },
        "Level 1 - Question 3" : {
          "answer" : "JOY",
          "correct" : "SURPRISE",
          "result" : "wrong"
        },
        "Level 1 - Question 4" : {
          "answer" : "SADNESS",
          "correct" : "SURPRISE",
          "result" : "wrong"
        },
        "Level 1 - Question 5" : {
          "answer" : "NEUTRAL_SLOW",
          "correct" : "FEAR",
          "result" : "wrong"
        },
        "Level 1 - Question 6" : {
          "answer" : "NEUTRAL_SLOW",
          "correct" : "NEUTRAL_SLOW",
          "result" : "correct"
        },
        "date" : {
          "date" : 11,
          "day" : 2,
          "hours" : 22,
          "minutes" : 1,
          "month" : 11,
          "nanos" : 176000000,
          "seconds" : 0,
          "time" : 1544562060176,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5342547801072894694,
        "mostSignificantBits" : -8076029385604120322,
        "score" : 40
      },
      "a2b71ab1-9f37-401f-8094-c8948ef3ece6" : {
        "Level 1 - Question 1" : {
          "answer" : "ANGER",
          "correct" : "ANGER",
          "result" : "correct"
        },
        "Level 1 - Question 2" : {
          "answer" : "DISGUST",
          "correct" : "SADNESS",
          "result" : "wrong"
        },
        "date" : {
          "date" : 12,
          "day" : 3,
          "hours" : 10,
          "minutes" : 14,
          "month" : 11,
          "nanos" : 546000000,
          "seconds" : 49,
          "time" : 1544606089546,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -9181493199922533146,
        "mostSignificantBits" : -6721874568644444129,
        "score" : 20
      },
      "b2d91ad4-ee3b-41d1-b921-765097233759" : {
        "score" : 0
      },
      "b5adc29b-e556-429a-8ab1-3b57f1a0f1f6" : {
        "score" : 0
      },
      "cd525f1e-2fda-4f86-b1fd-db710a30e5cf" : {
        "Level 1 - Question 1" : {
          "answer" : "FEAR",
          "correct" : "FEAR",
          "result" : "correct"
        },
        "Level 1 - Question 2" : {
          "answer" : "FEAR",
          "correct" : "FEAR",
          "result" : "correct"
        },
        "date" : {
          "date" : 11,
          "day" : 2,
          "hours" : 22,
          "minutes" : 3,
          "month" : 11,
          "nanos" : 539000000,
          "seconds" : 12,
          "time" : 1544562192539,
          "timezoneOffset" : -60,
          "year" : 118
        },
        "leastSignificantBits" : -5621095481339746865,
        "mostSignificantBits" : -3651751764587556986,
        "score" : 40
      },
      "totalScore" : 120
    },
    "country" : "United Kingdom",
    "email" : "mcswinterproject@gmail.com",
    "metadata" : {
      "creationTimestamp" : 1539878121000,
      "lastSignInTimestamp" : 1544603710000
    },
    "name" : "Mobile Communication Services"
  }
}
```

Now I know this might be a bit much to look at, especially since these are only 2 users and their data. However, we're only using part of the data:

- Total score(to display leaderboards)
- time(to display recently active)
- And a few more.

To access total score, we only need to access the RANKED array, since we will not be using practice results in any of our analysis.

In our code this is done:

```js

for(var i=0; i < array.length;i++){
      var currentArray = array[i];
  }
score = currentArray[1];

```
So basically we get an array of ranked games, looking at the JSON the second element in that array would be totalScore, while the first one [0] would be ranked games in an array.

Getting the timestamp is a bit more straight forward. We access the ranked games structure we've accessed before, then get every second element of it (the first element is always the ID of the element), and from that element we can access the object date, which contains the time object.

The time object is an Android generated timestamp that is UTC Universal Time. So a higher value of UTC means the higher the value the more recent the login was.

## Receiving and displaying data on the WebUI

Most logic is done through callbacks. Any time a new entry is registered on the Firebase, it sends an "event" that triggers the logic to append a new div into the HTML. Look at the MESSAGE_TEMPLATE variable for that.

So the call order would be:

-> Receive callback
-> Parse the data that was added
-> Turn the data into HTML code
-> Append HTML code

Reading through the docs, we see that every time a new child is created it returns the ENTIRE object, not just the new one.

The order of methods called:

->loadLeaderBoards
    -> displayLeaderBoards
->loadRecentlyActive
    ->displayPreviousUsers

