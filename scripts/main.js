
/**
 * Copyright 2018 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
//Country counter
var incrementor = 0;
// Signs-in Friendly Chat.
function signIn() {
	let provider = new firebase.auth.GoogleAuthProvider();
	firebase.auth().signInWithPopup(provider);
}
// Signs-out of Friendly Chat.
function signOut() {
	firebase.auth().signOut();
}
// Initiate firebase auth.
function initFirebaseAuth() {
	firebase.auth().onAuthStateChanged(authStateObserver);
}
// Returns the signed-in user's profile Pic URL.
function getProfilePicUrl() {
	return firebase.auth().currentUser.photoURL || '/images/profile_placeholder.png';
}
// Returns the signed-in user's display name.
function getUserName() {
	return firebase.auth().currentUser.displayName;
}
// Returns true if a user is signed-in.
function isUserSignedIn() {
	return !!firebase.auth().currentUser;
}
// Loads recently active users
function loadRecentlyActive() {
	// Loads the last 12 users based on the timestamp and listen for new ones.
	//This is triggered each time a new child is added, so every time a new user is registered
	let recentlyActive = firebase.database().ref('users').orderByChild('/RANKED/date/time').limitToLast(50).on('child_added', function (snapshot) {
		//let lastGame = firebase.database().ref('/users/game/').orderByChild('date/time').limitToFirst(1);
		let data = snapshot.val();

		let ranked = data.RANKED;
		//Score is currently a score for each game separatedly, so we have to sum/add it up ourselves
		let scoreX = 0;
		let rankedGames = Object.entries(ranked);
		let ranked_array = [];
		for (let key in rankedGames) {
			if (rankedGames.hasOwnProperty(key)) {
				ranked_array.push(rankedGames[key]);
			}
		}
		let placeholderarr = ranked_array;
		scoreX = ranked_array[ranked_array.length - 1];
		scoreX = scoreX[1];
		let timeArray = [];
		for (let i = 0; i < ranked_array.length - 3; i++) {
			let currRanked = placeholderarr[i];
			//Take the second elemeent out of each ranked object, and push it into an array then find the largest value1
			console.log(currRanked.length);
			console.log("THINGY");
			try {
				timeArray.push(currRanked[1].date.time);
			}
			catch (err) {
				if (err.name == "TypeError") {
					console.log("Someone hecked up in the database");
				}

			}
		}

		console.log(timeArray);
		let largestTime = Math.max(...timeArray);
		//Turn from UTC universal time to readable
		let date = new Date(largestTime);
		let formattedTime = date.toLocaleDateString() + " " + date.toLocaleTimeString();
		console.log("Dates");
		console.log(formattedTime);
		//@snap.key = used for user data, the html element right now
		//@data.id = id of the user
		//@data.name = user's name
		//@data.profilePicUrl = url of the user's profile picture
		//@formattedTime = formatted time from unix timestamp into readable
		//@scoreX = the user's score
		//We add a random character to snapshot key, so we can display identical users on different HTML elements
		displayPreviousUsers(snapshot.key + "a", data.id, data.name, data.profilePicUrl, formattedTime, scoreX);
	});
}
//Get data ordered by time, same as recently active users
//Display them in the same way we display recently active users
// This time, we dont need to add timestamps
function loadCountries() {
	//Get countries, turn it into a json file, show 
	let countryArr = [];
	let countries = firebase.database().ref("users").once('value', function (snapshot) {
		snapshot.forEach(function (child) {
			countryArr.push(child.val().country);
		});
		//Counts uniques and puts them in a map, so United Kingdom, 2 , 2 being the amount of times United Kingdom is in the array
		var map = countryArr.reduce(function (prev, curr) {
			prev[curr] = (prev[curr] || 0) + 1;
			return prev;
		}, {});
		console.log(map);
		var keyArr = Object.keys(map);
		var valArr = Object.values(map);
		console.log(keyArr);
		console.log(valArr);
		var newArr = [];
		for(var i = 0; i < keyArr.length; i++){
			newArr.push({"label" : keyArr[i], "value" : valArr[i]});
		}
		console.log(newArr);
		var chartInstance = new FusionCharts({
			type: 'pie3d',
			width: '700', // Width of the chart
			height: '400', // Height of the chart
			dataFormat: 'json', // Data type
			renderAt: 'chart-container', //container where the chart will render
			dataSource: {
				"chart": {
					"caption": "Users of EmotiQuiz by Country",
					"subCaption": "By amount of users",
					"xAxisName": "Country",
					"yAxisName": "Number of users",
					"theme": "fusion",
				},
				// Chart Data
				"data": newArr
			}
		});
		// Render
		chartInstance.render();
	});
}
//Loads up leaderboards, sorts by SCORE, displays user data the same way as loadRecentlyActive()
//Instead of showing the timestamp we show the score, otherwise the methods are identical mostly
function loadLeaderboards() {
	//We load leaderboards once because we're only prepending HTML, so if we prepended a new leaderboard every time a new user is registered it would appear at the top of the list.
	let leaderboards = firebase.database().ref('users').orderByChild('RANKED/score').limitToLast(50).on('child_added', function (snapshot) {
		let data = snapshot.val();
		let ranked = data.RANKED;
		console.log("Ranked");
		console.log(ranked);
		console.log("Children");
		let rankedGames = Object.entries(ranked);
		let ranked_array = [];
		for (let key in rankedGames) {
			if (rankedGames.hasOwnProperty(key)) {
				ranked_array.push(rankedGames[key]);
			}
		}
		console.log(ranked_array);
		let score = ranked_array[ranked_array.length - 1];
		score = score[1];
		console.log("SCORE IS " + score);
		displayLeaderBoards(snapshot.key, data.id, data.name, data.profilePicUrl, score);
	});
}
// Triggers when the auth state change for instance when the user signs-in or signs-out.
function authStateObserver(user) {
	if (user) { // User is signed in!
		// Get the signed-in user's profile pic and name.
		let profilePicUrl = getProfilePicUrl();
		let userName = getUserName();
		// Set the user's profile pic and name.
		userPicElement.style.backgroundImage = 'url(' + profilePicUrl + ')';
		userNameElement.textContent = userName;
		// Show user's profile and sign-out button.
		userNameElement.removeAttribute('hidden');
		userPicElement.removeAttribute('hidden');
		signOutButtonElement.removeAttribute('hidden');
		// Hide sign-in button.
		signInButtonElement.setAttribute('hidden', 'true');
		// We save the Firebase Messaging Device token and enable notifications.
	} else { // User is signed out!
		// Hide user's profile and sign-out button.
		userNameElement.setAttribute('hidden', 'true');
		userPicElement.setAttribute('hidden', 'true');
		signOutButtonElement.setAttribute('hidden', 'true');
		// Show sign-in button.
		signInButtonElement.removeAttribute('hidden');
	}
}
// Resets the given MaterialTextField.
// Template for messages.
let MESSAGE_TEMPLATE =
	'<div class="message-container">' +
	'<div class="spacing"><div class="pic"></div></div>' +
	'<div class="message"></div>' +
	'<div class="name"></div>' +
	'</div>';
// A loading image URL.
//let LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif?a';
// Displays previous users in the UI.
//Key,name,text,picURL serves the same purpose
function displayPreviousUsers(key, name, text, picUrl, date, score) {
	console.log("KEYFORUSER");
	console.log(key);
	let div = document.getElementById(key);
	// If an element for that message does not exists yet we create it.
	//We added an a character to the key, because this query looks through all elements
	//Without adding a the same person(with the identical ID) cannot show up on both sides
	if (!div) {
		let container = document.createElement('div');
		console.log("Previous users div missing");
		container.innerHTML = MESSAGE_TEMPLATE;
		div = container.firstChild;
		div.setAttribute('id', key);
		//Make sure to prepend the new node, because
		messageListElement.parentElement.append(div);
	}
	div.querySelector('.name').textContent = name;
	let messageElement = div.querySelector('.message');
	if (text) { // If the message is text.
		//if there's a date to display, display it, if not we're only displaying the score
		messageElement.textContent = text + "\nLast played on: " + date + "\n Score: " + score;
		// Replace all line breaks by <br>.
		messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
	}
	// Show the card fading-in and scroll to view the new message.
	setTimeout(function () {
		div.classList.add('visible')
	}, 1);
	messageListElement.scrollTop = messageListElement.scrollHeight;
}
function displayLeaderBoards(key, name, text, picUrl, score) {
	let div = document.getElementById(key);
	// If an element for that message does not exists yet we create it.
	if (!div) {
		console.log("Leaderboard div missing");
		let container = document.createElement('div');
		container.innerHTML = MESSAGE_TEMPLATE;
		div = container.firstChild;
		div.setAttribute('id', key);
		leaderBoardListElement.parentElement.prepend(div);
	}
	div.querySelector('.name').textContent = name;
	let messageElement = div.querySelector('.message');
	if (text) { // If the message is text.
		messageElement.textContent = text + "\n Score: " + score;
		// Replace all line breaks by <br>.
		messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
	}
	// Show the card fading-in and scroll to view the new message.
	setTimeout(function () {
		div.classList.add('visible')
	}, 1);
	leaderBoardListElement.scrollTop = leaderBoardListElement.scrollHeight;
}
function displayCountries(key, country) {

}
// Enables or disables the submit button depending on the values of the input
// fields.
// Checks that the Firebase SDK has been correctly setup and configured.
function checkSetup() {
	if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
		window.alert('You have not configured and imported the Firebase SDK. ' +
			'Make sure you go through the codelab setup instructions and make ' +
			'sure you are running the codelab using `firebase serve`');
	}
}
// Checks that Firebase has been imported.
checkSetup();
// Shortcuts to DOM Elements.
let messageListElement = document.getElementById('messages');
let leaderBoardListElement = document.getElementById('messages2');
let userPicElement = document.getElementById('user-pic');
let userNameElement = document.getElementById('user-name');
let signInButtonElement = document.getElementById('sign-in');
let signOutButtonElement = document.getElementById('sign-out');
let signInSnackbarElement = document.getElementById('must-signin-snackbar');
// Saves message on form submit.
signOutButtonElement.addEventListener('click', signOut);
signInButtonElement.addEventListener('click', signIn);
// Toggle for the button.
// Events for image upload.
// initialize Firebase
initFirebaseAuth();
// We load currently existing chat messages and listen to new ones.
loadLeaderboards();
loadRecentlyActive();
loadCountries();